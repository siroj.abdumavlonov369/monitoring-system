package com.example.monitoringsystem.entity;

public enum RoleName {
    USER, ADMIN, SUPER_ADMIN
}