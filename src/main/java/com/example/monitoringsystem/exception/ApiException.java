package com.example.monitoringsystem.exception;

public record ApiException(String error) {
}
