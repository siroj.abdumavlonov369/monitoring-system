package com.example.monitoringsystem.constants;

public class RequestStatus {
    public static final String WAITING = "WAITING";
    public static final String ACCEPTED = "ACCEPTED";
    public static final String DECLINED = "DECLINED";

}
