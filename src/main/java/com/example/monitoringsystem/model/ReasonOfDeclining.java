package com.example.monitoringsystem.model;


public record ReasonOfDeclining(String id, String message, String declinedUserId) {
}
