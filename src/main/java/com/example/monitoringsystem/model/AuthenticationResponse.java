package com.example.monitoringsystem.model;

public record AuthenticationResponse(String token) {
}
