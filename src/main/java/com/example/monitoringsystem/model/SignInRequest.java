package com.example.monitoringsystem.model;


public record SignInRequest(String id, String password) {
}
