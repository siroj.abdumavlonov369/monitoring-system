package com.example.monitoringsystem.model;


public record ExactColumnsDTO(String id, int bankomats,
                              int computers, int keyboard, int printer,int mouse,
                              int monitor, int employees) {
}
