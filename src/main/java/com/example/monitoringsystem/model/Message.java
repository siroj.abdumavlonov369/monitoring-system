package com.example.monitoringsystem.model;



public record Message( String typeOfMessage, String content) {
}
