package com.example.monitoringsystem.model;

public record ApiResponse(String message) {
}
