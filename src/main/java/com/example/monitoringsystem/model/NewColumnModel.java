package com.example.monitoringsystem.model;


public record NewColumnModel(int value, String columnName) {

}
