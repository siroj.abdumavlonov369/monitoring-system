package com.example.monitoringsystem.model;



import java.time.LocalDate;


public record FromAndToDates(LocalDate from, LocalDate to) {

}
