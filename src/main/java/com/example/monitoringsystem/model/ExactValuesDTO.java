package com.example.monitoringsystem.model;

public record ExactValuesDTO(String id, int bankomats,
                             int computers, int keyboard, int printer,int mouse,
                             int monitor, int employees) {
}
