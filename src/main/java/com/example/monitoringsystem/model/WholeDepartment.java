package com.example.monitoringsystem.model;


public record WholeDepartment<T, I>(T exactColumns, I efficiency) {
}
