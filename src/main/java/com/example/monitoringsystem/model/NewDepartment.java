package com.example.monitoringsystem.model;




public record NewDepartment(String id, String departmentName, String address,
                            double lon, double lat, String idOfMainBranch) {
}
